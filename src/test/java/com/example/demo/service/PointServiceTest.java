package com.example.demo.service;

import com.example.demo.dto.PointBody;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Point;
import com.example.demo.repository.PointRepository;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PointServiceTest {

    private final PointRepository pointRepository = mock(PointRepository.class);
    private final PointService pointService = new PointService(pointRepository);

    double x = 123.234;
    double y = 123.234;
    String name = "Tom";
    String color = "red";

    double x1 = 34.234;
    double y1 = 345.34;
    String name1 = "Kled";
    String color1 = "black";

    Point point = new Point(x, y, name, color);
    Point point1 = new Point(x1, y1, name1, color1);

    PointBody pointBody = new PointBody(x, y, name, color);
    PointBody pointBody1 = new PointBody(x1, y1, name1, color1);

    List<Point> points = new ArrayList<>();
    List<PointBody> pointBodies = new ArrayList<>();


    @Test
    void testGetAllProducts() {
        points.add(point);
        points.add(point1);
        pointBodies.add(pointBody);
        pointBodies.add(pointBody1);

        when(pointRepository.findAllPoints()).thenReturn(points);

        List<PointBody> allPoints = pointService.getPoints();

        assertAll("Verify points information",
                () -> assertNotNull(pointBodies),
                () -> assertEquals(2, pointBodies.size()),
                () -> assertEquals(allPoints.get(0).getX(), pointBodies.get(0).getX()),
                () -> assertEquals(allPoints.get(0).getY(), pointBodies.get(0).getY()),
                () -> assertEquals(allPoints.get(0).getName(), pointBodies.get(0).getName()),
                () -> assertEquals(allPoints.get(0).getColor(), pointBodies.get(0).getColor())
        );

    }

    @Test
    void testGetAllPointsWhenNoPointsFound() {
        when(pointRepository.findAllPoints()).thenReturn(Collections.emptyList());

        NotFoundException ex = assertThrows(NotFoundException.class, () ->
                Objects.requireNonNull(pointService.getPoints()));

        assertEquals(ex.getMessage(), "No points exist");
    }

    @Test
    void testCreatePoints() {
        points.add(point);
        points.add(point1);
        pointBodies.add(pointBody);
        pointBodies.add(pointBody1);

        when(pointRepository.save(point)).thenReturn(point);
        when(pointRepository.save(point1)).thenReturn(point1);

        List<PointBody> createdPoints = pointService.createPoints(points);

        assertAll("Verify points information",
                () -> assertNotNull(createdPoints),
                () -> assertEquals(2, createdPoints.size()),
                () -> assertEquals(createdPoints.get(0).getX(), pointBodies.get(0).getX()),
                () -> assertEquals(createdPoints.get(0).getY(), pointBodies.get(0).getY()),
                () -> assertEquals(createdPoints.get(0).getName(), pointBodies.get(0).getName()),
                () -> assertEquals(createdPoints.get(0).getColor(), pointBodies.get(0).getColor())
        );
    }

    @Test
    void testCreatePointsWhenListIsEmpty() {

        List<Point> points = new ArrayList<>();

        BadRequestException ex = assertThrows(BadRequestException.class, () ->
                pointService.createPoints(points));

        assertEquals(ex.getMessage(), "Missing points information: " + points);
    }

}