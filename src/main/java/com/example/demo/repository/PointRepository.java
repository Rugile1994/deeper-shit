package com.example.demo.repository;

import com.example.demo.model.Point;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PointRepository extends CrudRepository<Point, Long> {

    @Query(value = "select * from point", nativeQuery = true)
    List<Point> findAllPoints();
}
