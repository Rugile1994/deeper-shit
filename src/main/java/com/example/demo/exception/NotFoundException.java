package com.example.demo.exception;


import java.io.Serial;

public class NotFoundException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 834381472668788549L;

    public NotFoundException(String message) {
        super(message);
    }
}
