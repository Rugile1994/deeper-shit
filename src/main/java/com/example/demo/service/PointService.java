package com.example.demo.service;

import com.example.demo.dto.PointBody;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Point;
import com.example.demo.repository.PointRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PointService {

    private final PointRepository pointRepository;

    public PointService(PointRepository pointRepository) {
        this.pointRepository = pointRepository;
    }

    public List<PointBody> getPoints() {
        List<PointBody> allPoints = pointRepository.findAllPoints().stream()
                .map(point -> new PointBody(point.getX(), point.getY(), point.getName(), point.getColor()))
                .collect(Collectors.toList());

        if (allPoints.isEmpty()) {
            throw new NotFoundException("No points exist");
        }
        return allPoints;
    }

    public List<PointBody> createPoints(List<Point> points) {
        if (points.isEmpty()) {
            throw new BadRequestException("Missing points information: " + points);
        }

        return points.stream().map(pointRepository::save)
                .map(point -> new PointBody(point.getX(), point.getY(), point.getName(), point.getColor()))
                .collect(Collectors.toList());
    }

    public void deletePoint(Long pointId) {
        pointRepository.findById(pointId)
                .orElseThrow(() -> new NotFoundException("Id doesn't exist: " + pointId));
        pointRepository.deleteById(pointId);
    }
}
