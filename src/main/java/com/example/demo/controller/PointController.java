package com.example.demo.controller;

import com.example.demo.dto.PointBody;
import com.example.demo.model.Point;
import com.example.demo.service.PointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class PointController {
    private final PointService pointService;

    public PointController(PointService pointService) {
        this.pointService = pointService;
    }

    @GetMapping(value = "/points")
    public ResponseEntity<List<PointBody>> getAllPoints() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(pointService.getPoints());
    }

    @PostMapping(value = "/points")
    public ResponseEntity<List<PointBody>> createPoints(@Valid @RequestBody ArrayList<Point> points) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(pointService.createPoints(points));
    }

    @DeleteMapping(value = "/points/{pointId}")
    public ResponseEntity<?> deletePoint(@PathVariable Long pointId) {
        pointService.deletePoint(pointId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
