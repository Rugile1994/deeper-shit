package com.example.demo.dto;

public class PointBody {

    private final double x;
    private final double y;
    private final String name;
    private final String color;

    public PointBody(double x, double y, String name, String color) {
        this.x = x;
        this.y = y;
        this.name = name;
        this.color = color;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
